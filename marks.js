function calculateGrade(){
	var mathsMarks = parseInt(document.getElementById("maths").value);
	var hindiMarks = parseInt(document.getElementById("hindi").value);
  var englishMarks = parseInt(document.getElementById("english").value);
  var scienceMarks = parseInt(document.getElementById("science").value);
  var text = "You entered wrong input for ";
  var percentage = 0;

  if (!validInput(mathsMarks)){
    document.getElementById("text").innerHTML = text + "Maths marks"
    return
  }
  if (!validInput(hindiMarks)){
    document.getElementById("text").innerHTML = text + "Hindi marks"
    return
  }
  if (!validInput(englishMarks)){
    document.getElementById("text").innerHTML = text + "English marks"
    return
  }
  if (!validInput(scienceMarks)){
    document.getElementById("text").innerHTML = text + "Science marks"
    return
  }
  percentage = (mathsMarks+hindiMarks+englishMarks+scienceMarks)/4;
  document.getElementById("text").innerHTML = assignGrade(percentage)     
}

function validInput(num){
  if (isNaN(num) == false && !(num == "") && num >= 0 && num <= 100){
    return true;
  }
  else{
    return false;
  }
}

function assignGrade(number){
  if (number>= 90 && number <= 100){
    return "Your grade is A"
  }
  if(number >= 80 && number < 90)
    return "Your grade is B"
  if (number >= 70 && number < 80){
    return "Your grade is C "
  }
  if (number >= 60 && number < 70){
    return "Your grade is D"
  }
  return "Your grade is F"
}